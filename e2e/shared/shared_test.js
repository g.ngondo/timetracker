/* global describe, beforeEach, it, browser, expect */
'use strict';

var SharedPagePo = require('./shared.po');

describe('Shared page', function () {
  var sharedPage;

  beforeEach(function () {
    sharedPage = new SharedPagePo();
    browser.get('/#/shared');
  });

  it('should say SharedCtrl', function () {
    expect(sharedPage.heading.getText()).toEqual('shared');
    expect(sharedPage.text.getText()).toEqual('SharedCtrl');
  });
});
