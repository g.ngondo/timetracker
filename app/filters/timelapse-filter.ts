///<reference path='../../typings/tsd.d.ts' />
module timelapse {
  'use strict';

  /**
  * @ngdoc filter
  * @name home.filter:timelapse
  *
  * @description
  *
  * @param {Array} input The array to filter
  * @returns {Array} The filtered array
  *
  */
  angular
    .module('home')
    .filter('timelapse', timelapse);

  function timelapse() {
    return function (duration: number) {
      if (duration == null) {
        return '';
      }
      var hours;
      var seconds;
      var minutes;
      
      seconds = duration % 60;
      minutes = Math.floor(duration / 60) % 60;
      hours = Math.floor(duration / (60 * 60));
      
      hours = (hours < 10) ? '0' + hours : hours;
      minutes = (minutes < 10) ? '0' + minutes : minutes;
      seconds = (seconds < 10) ? '0' + seconds : seconds;
      
      return hours + ':' + minutes + ':' + seconds;
    };
  }
}
