///<reference path='../../typings/tsd.d.ts' />

/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('timelapse', function () {
  beforeEach(angular.mock.module('home'));

  it('should filter a number into duration', inject(function ($filter) {
    expect($filter('timelapse')(60)).toEqual('00:01:00');
    expect($filter('timelapse')(50)).toEqual('00:00:50');
  }));

  it('should not convert days into duration', inject(function ($filter) {
    expect($filter('timelapse')(86401)).toEqual('24:00:01');
  }));

});
