///<reference path='../typings/tsd.d.ts' />
module timetracker {
  'use strict';

  /* @ngdoc object
   * @name timetracker
   * @description
   *
   */
  angular
    .module('timetracker', [
      'ui.router',
      'ui.bootstrap',
      'home',
      'shared'
    ]);
}
