///<reference path='../../typings/tsd.d.ts' />


module HomeCtrl {
  'use strict';

  class TaskMeasure {
    timeElapsed: number;
    startTime: number;
    endTime: number;
    constructor() {
      this.startTime = new Date().getTime();
      this.endTime = 0;
      this.timeElapsed = 0;
    }
  }

  class Task { 
    taskName: string;
    taskMeasures: Array<TaskMeasure>;
    taskLastMeasure: number;
    taskTotalTime: number;
    constructor() {
      this.taskMeasures = new Array<TaskMeasure>();
    }
  }

  class HomeCtrl {
    ctrlName: string;


    
    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject: Array<string> = [
      '$scope',
      'Persisting'
    ];

    // dependencies are injected via AngularJS $injector
    constructor(private $scope: ng.IScope, private Persisting) {
      var vm = this;
      vm.ctrlName = 'HomeCtrl';

      $scope.tasks = Persisting.getAll();
      $scope.newTask = new Task();
      $scope.currentTaskMeasure = new TaskMeasure();

      //Persist Tasks
      $scope.$on('tracking:persist', function(e, data) {
        if (data != undefined && $scope.currentTask != undefined) {
          $scope.currentTaskMeasure.timeElapsed = data.elapsed;
          $scope.currentTaskMeasure.endTime = data.end;
          $scope.currentTask.taskMeasures.push($scope.currentTaskMeasure);
          
          $scope.newTask = new Task();
          $scope.currentTaskMeasure = new TaskMeasure();
          Persisting.saveTask($scope.currentTask);
        }
      });

      $scope.startNewTask = function(newTask) {
        
        

        var exists=false;
        for(var i=0; i < $scope.tasks.length; i++){
            if ($scope.tasks[i].taskName === newTask.taskName) {
                newTask = $scope.tasks[i];
                exists = true;
                break;
            }
        }
        if (!exists){
          $scope.tasks.push(newTask);
        }
        $scope.$broadcast ('stopwatch:end');
        
        $scope.currentTask = newTask;
        $scope.$broadcast('stopwatch:start', newTask);
      }
    }
    
  }

  /**
  * @ngdoc object
  * @name home.controller:HomeCtrl
  *
  * @description
  *
  */
  angular
    .module('home')
    .controller('HomeCtrl', HomeCtrl);
}
