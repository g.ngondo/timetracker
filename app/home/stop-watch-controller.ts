///<reference path='../../typings/tsd.d.ts' />

module StopWatchCtrl {
  'use strict';

  class StopWatchCtrl {
    ctrlName: string;
   
    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject: Array<string> = [
      '$scope',
      '$timeout',
    ];

    // dependencies are injected via AngularJS $injector
    constructor($scope: ng.IScope, $timeout: ng.ITimeoutService) {
      var vm = this;
      vm.ctrlName = 'StopWatchCtrl';
  
      $scope.start = function() {
        $scope.running = true;
        countdown();
        $scope.$emit ('tracking:persist');
      };
  
      
      $scope.pause = function() {
        $scope.running = false;
        
        $timeout.cancel($scope.timeout);
      };

      $scope.stop = function() {
        $scope.running = false;
        $timeout.cancel($scope.timeout);
        
        $scope.$emit ('tracking:persist', {'end':new Date().getTime(), 'elapsed':$scope.timeElapsed});
        
      };
  
      $scope.$on('stopwatch:start', function(e, data) {
        console.log('stopwatch start data',data);
        $scope.timeElapsed = 0;
        if ( data.taskMeasures != null && data.taskMeasures.length > 0 ) {
          $scope.timeElapsed = data.taskMeasures[data.taskMeasures.length - 1].timeElapsed;
        }
        $scope.start();
      });
  
      $scope.$on('stopwatch:end', function(e) {
        console.log($scope);
        $scope.stop();
      });
  
      function countdown() {
        $scope.timeElapsed++;
        $scope.timeout = $timeout(countdown, 1000);
      }
  
     
    }

 
   

    

  }


  /**
  * @ngdoc object
  * @name home.controller:StopWatchCtrl
  *
  * @description
  *
  */
  angular
    .module('home')
    .controller('StopWatchCtrl', StopWatchCtrl);
}
