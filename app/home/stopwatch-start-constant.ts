///<reference path='../../typings/tsd.d.ts' />
module stopwatchStart {
  'use strict';

  /**
  * @ngdoc service
  * @name home.constant:stopwatchStart
  *
  * @description
  *
  */
  angular
    .module('home')
    .constant('stopwatchStart', 'stopwatch:start');
}
