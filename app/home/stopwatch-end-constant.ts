///<reference path='../../typings/tsd.d.ts' />
module stopwatchEnd {
  'use strict';

  /**
  * @ngdoc service
  * @name home.constant:stopwatchEnd
  *
  * @description
  *
  */
  angular
    .module('home')
    .constant('stopwatchEnd', 'stopwatch:end');
}
