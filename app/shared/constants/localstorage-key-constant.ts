///<reference path='../../../typings/tsd.d.ts' />
module localstorageKey {
  'use strict';

  /**
  * @ngdoc service
  * @name shared.constant:localstorageKey
  *
  * @description
  *
  */
  angular
    .module('shared')
    .constant('localstorageKey', 'timetrackerData');
}
