///<reference path='../../typings/tsd.d.ts' />
module shared {
  'use strict';

  /* @ngdoc object
  * @name shared
  * @description
  *
  */
  angular
    .module('shared', [
      'ui.router'
    ]);
}
