///<reference path='../../typings/tsd.d.ts' />

module Persisting {
  'use strict';

  class Persisting {
    localstorageKey: string;
    count: number;

    public static $inject: Array<string> = [
      'localstorageKey'
    ];
    
    constructor(localstorageKey) {
      var vm = this;
      vm.localstorageKey = localstorageKey;
      vm.count = 0;
    }
    
    saveTask(task) {
      var vm = this;
      vm.count++;
      localStorage.setItem(vm.localstorageKey + '_'+task.taskName, JSON.stringify(task.taskMeasures));
    }

    saveTasks(tasks) {
      var vm = this;
      localStorage.setItem(vm.localstorageKey, JSON.stringify(tasks));
    }
    
    getAll() {
      var vm = this;
      var items = [];

      for (var i = 0; i < vm.count; i++) {
        items.push(localStorage.getItem(vm.localstorageKey + '_' + i));
      };

      return items;
    }
  }
  /**
   * @ngdoc service
   * @name home.service:Persisting
   *
   * @description
   *
   */
  angular
    .module('timetracker')
    .service('Persisting', Persisting);
}
