///<reference path='../../typings/tsd.d.ts' />
module SenseDesktop32 {
  'use strict';

  class SenseDesktop32 {
    public static $inject: Array<string> = [
    ];

    constructor() {
    }

    get(): string {
      return 'SenseDesktop32';
    }
  }

  /**
   * @ngdoc service
   * @name home.service:SenseDesktop32
   *
   * @description
   *
   */
  angular
    .module('home')
    .service('SenseDesktop32', SenseDesktop32);
}
