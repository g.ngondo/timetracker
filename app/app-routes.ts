///<reference path='../typings/tsd.d.ts' />
module timetracker {
  'use strict';

  angular
    .module('timetracker')
    .config(config);

  function config($urlRouterProvider: ng.ui.IUrlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
  }
}
